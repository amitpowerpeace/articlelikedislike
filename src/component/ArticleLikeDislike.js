import React, { Component } from 'react';
import '../App.css';

class ArticleLikeDislike extends Component {
	constructor(props) {
		super(props);
		this.state = {
			likes: 100,
			dislikes: 25,
			counter: false,
			dislikeCnt: false
		};
	}
	likesArticle = () => {
		if (this.state.counter == false) {
			this.setState({
				likes: this.state.likes + 1,
				counter: !this.state.counter
			});
		} else {
			this.setState({
				likes: this.state.likes - 1,
				counter: !this.state.counter
			});
		}
	};
	dislikesArticle = () => {
		this.setState({
			dislikes: this.state.dislikes - 1,
			dislikeCnt: !this.state.dislikeCnt
		});
	};

	render() {
		return (
			<div>
				<button
					className={this.state.counter ? 'like-button liked' : 'like-button'}
					onClick={this.likesArticle}
				>
					Like | <span className="likes-counter">{this.state.likes}</span>
				</button>
				<button
					className={this.state.dislikeCnt ? 'like-button disliked' : 'like-button '}
					onClick={this.dislikesArticle}
				>
					Dislike | <span className="dislikes-counter">{this.state.dislikes}</span>
				</button>
			</div>
		);
	}
}

export default ArticleLikeDislike;
