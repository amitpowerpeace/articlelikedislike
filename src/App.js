import React, { Component } from 'react';
import ArticleLikeDislike from './component/ArticleLikeDislike'

class App extends Component {
	constructor(props) {
		super(props);
		
	} 
	render() {
		return (
			<div>
				<h2>Like/Dislike</h2>
				<ArticleLikeDislike/>
			</div>
		);
	}
}

export default App;
